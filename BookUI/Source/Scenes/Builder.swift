//
//  Builder.swift
//  BookUI
//
//  Created by HahnDante on 25/06/2019.
//  Copyright © 2019 한상민. All rights reserved.
//

import Foundation
import UIKit

class SceneBuilder {
    
    private static let storyboard = "Main"
    
    static func SearchResultViewController() -> SearchResultViewController {
        let viewController = UIStoryboard(name: SceneBuilder.storyboard, bundle: nil).instantiateViewController(withIdentifier: "SearchResultViewController") as! SearchResultViewController
        return viewController
    }
    
    static func DetailViewController() -> DetailViewController {
        let viewController = UIStoryboard(name: SceneBuilder.storyboard, bundle: nil).instantiateViewController(withIdentifier: "DetailViewController") as! DetailViewController
        return viewController
    }
}
