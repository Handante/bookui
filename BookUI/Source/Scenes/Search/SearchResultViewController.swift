//
//  SearchResultViewController.swift
//  BookUI
//
//  Created by neolyra on 25/06/2019.
//  Copyright © 2019 한상민. All rights reserved.
//

import UIKit

class SearchResultViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    private lazy var dataController = SearchDataResultController(scrollView: self.tableView)
    
    override var navigationController: UINavigationController? {
        return presentingViewController?.navigationController
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configureTableView()
    }       
    
    func configureTableView() {
        self.tableView.register(UINib(nibName: "SearchResultTableViewCell", bundle: nil), forCellReuseIdentifier: "SearchResultTableViewCell")
        self.tableView.rowHeight  = 200
    }
    
    private func reset() {
        dataController.reset()
        tableView.tableHeaderView = nil
        tableView.reloadData()
    }

}

extension SearchResultViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataController.results.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let result = dataController.results[indexPath.row]
        let cell = tableView.dequeReusableCell(type: SearchResultTableViewCell.self, indexPath: indexPath)
        cell.configure(item: result)
        return cell
    }
}

extension SearchResultViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let result = dataController.results[indexPath.item]
        let detailViewController = SceneBuilder.DetailViewController()
        detailViewController.dataController = DetailDataController(identifier: result.contentIdentifier!)
        navigationController?.pushViewController(detailViewController, animated: true)
    }
}

extension SearchResultViewController: UISearchBarDelegate {
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        guard let query = searchBar.text, !query.isEmpty else { return }
        dataController.search(query: query, page: 1) { [weak self] isFirstLoading in
            guard let `self` = self else { return }
            self.tableView.reloadData()
            if self.dataController.results.isEmpty {
                
            }
            
            if isFirstLoading {
                Animator.defaultListAnimation(targetViews: self.tableView.visibleCells)
            }
        }
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        reset()
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchText.isEmpty {
            reset()
        }
    }
}
