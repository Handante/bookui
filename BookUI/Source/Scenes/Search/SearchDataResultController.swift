//
//  SearchDataResultController.swift
//  BookUI
//
//  Created by neolyra on 25/06/2019.
//  Copyright © 2019 한상민. All rights reserved.
//

import Foundation
import UIKit

class SearchDataResultController {
    
    var results: [Presentable] = []
    private var dataLoader: BookListDataLoader<Book>?
    
    init(scrollView: UIScrollView) {
        dataLoader = BookListDataLoader<Book>(scrollView: scrollView, start: 1, display: 100)
    }
    
    func reset() {
        results.removeAll()
        dataLoader?.reset()
    }
    
    func search(query: String, page: Int, completion: @escaping((Bool)->(Void))) {
        // 최근 검색어 추가
        RecentQueryManager.add(query: query)
        
        dataLoader?.fetch = { start, display, callBack in
            let target = SearchTarget.search(query: query, page: page, start: start, display: display)
            NetworkRequest.request(target: target, completion: callBack)
        }
        
        dataLoader?.completion = { [weak self] list in
            let isFirstLoading = self?.results.isEmpty ?? false
            self?.results.append(contentsOf: list.books)
            completion(isFirstLoading)
        }
        dataLoader?.load()
        
    }
    
}
