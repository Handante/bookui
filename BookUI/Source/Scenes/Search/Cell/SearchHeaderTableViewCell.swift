//
//  SearchHeaderTableViewCell.swift
//  BookUI
//
//  Created by neolyra on 25/06/2019.
//  Copyright © 2019 한상민. All rights reserved.
//

import UIKit

class SearchHeaderTableViewCell: UITableViewCell {

    @IBOutlet weak var searchBar: UISearchBar!
    
}
