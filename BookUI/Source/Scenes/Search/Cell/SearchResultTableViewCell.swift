//
//  SearchResultTableViewCell.swift
//  BookUI
//
//  Created by neolyra on 25/06/2019.
//  Copyright © 2019 한상민. All rights reserved.
//

import UIKit

class SearchResultTableViewCell: UITableViewCell, ReusableView {
    static var identifier: String = "SearchResultTableViewCell"
    
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var subTitle: UILabel!
    @IBOutlet weak var thumbnailImageView: UIImageView!
    
    func configure(item: Presentable) {
        self.title.text = item.contentTitle
        self.subTitle.text = item.contentSubTitle
        self.thumbnailImageView.setImage(with: item.contentImageURL)
    }
    
}
