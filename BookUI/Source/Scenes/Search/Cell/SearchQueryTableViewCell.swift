//
//  SearchQueryTableViewCell.swift
//  BookUI
//
//  Created by neolyra on 25/06/2019.
//  Copyright © 2019 한상민. All rights reserved.
//

import UIKit

class SearchQueryTableViewCell: UITableViewCell, ReusableView {
    
    static var identifier: String = "SearchQueryTableViewCell"
    
    
    @IBOutlet weak var queryLabel: UILabel!
    
}
