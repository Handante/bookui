//
//  SearchDataController.swift
//  BookUI
//
//  Created by neolyra on 25/06/2019.
//  Copyright © 2019 한상민. All rights reserved.
//

import Foundation

class SearchDataController {
    
    var queries: [String] {
        return RecentQueryManager.queries()
    }
}
