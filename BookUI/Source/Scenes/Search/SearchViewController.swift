//
//  SearchViewController.swift
//  BookUI
//
//  Created by HahnDante on 19/06/2019.
//  Copyright © 2019 한상민. All rights reserved.
//

import UIKit

class SearchViewController: UIViewController {
    
    private let dataController = SearchDataController()
    private let resultViewController = SceneBuilder.SearchResultViewController()
    private lazy var searchController = UISearchController(searchResultsController: resultViewController)

    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureSearchController()
        configureTableView()
        configureNavigationController()
        NotificationCenter.default.addObserver(self, selector: #selector(refresh), name: .DidChangeRecentQueries, object: nil)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self, name: .DidChangeRecentQueries, object: nil)

    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    private func configureNavigationController() {
        self.navigationController?.navigationBar.prefersLargeTitles = true
        title = "Search"
    }
    
    private func configureSearchController() {
        searchController.searchBar.delegate = resultViewController
        searchController.searchBar.placeholder = "Search"
        navigationItem.searchController = searchController
        self.definesPresentationContext = true
    }
    
    func configureTableView() {
        self.tableView.register(UINib(nibName: "SearchQueryTableViewCell", bundle: nil),
                                forCellReuseIdentifier: "SearchQueryTableViewCell")
        self.tableView.rowHeight  = UITableView.automaticDimension
    }
    
    @objc private func refresh() {
        tableView.reloadData()
    }
    
}

extension SearchViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataController.queries.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeReusableCell(type: SearchQueryTableViewCell.self, indexPath: indexPath)
        cell.queryLabel.text = dataController.queries[indexPath.item]
        return cell
    }
}

extension SearchViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        searchController.isActive = true
        searchController.searchBar.text = dataController.queries[indexPath.item]
        print(dataController.queries[indexPath.row])
        resultViewController.searchBarSearchButtonClicked(searchController.searchBar)
    }
}





