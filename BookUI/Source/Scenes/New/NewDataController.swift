//
//  NewDataController.swift
//  BookUI
//
//  Created by HahnDante on 23/06/2019.
//  Copyright © 2019 한상민. All rights reserved.
//

import UIKit

class NewDataController {
    
    var results: [Presentable] = []
    private var dataLoader: BookListDataLoader<Book>?
    
    init(scrollView: UIScrollView) {
        dataLoader = BookListDataLoader<Book>(scrollView: scrollView, start: 1, display: 10)
    }
    
    func reset() {
        results.removeAll()
        dataLoader?.reset()
    }
    
    func request(_ completion: @escaping(Bool) -> Void) {
        dataLoader?.fetch = { (start, display, callback) in
            let target = NewTarget.new(start: start, dispaly: display)
            NetworkRequest.request(target: target, completion: callback)
        }
            
        dataLoader?.completion = { [weak self] list in
            let isFirstLoading = self?.results.isEmpty ?? false
            self?.results.append(contentsOf: list.books)         
            completion(isFirstLoading)
        }
        
        dataLoader?.load()
    }
}
