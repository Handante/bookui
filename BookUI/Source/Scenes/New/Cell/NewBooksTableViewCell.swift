//
//  NewBooksTableViewCell.swift
//  BookUI
//
//  Created by neolyra on 24/06/2019.
//  Copyright © 2019 한상민. All rights reserved.
//

import UIKit

class NewBooksTableViewCell: UITableViewCell, ReusableView {
    
    @IBOutlet weak var thumbnailImageView: UIImageView!
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var subTitle: UILabel!
    
    static var identifier: String = "NewBooksTableViewCell"
    
    func setup(item: Presentable) {
        title.text = item.contentTitle
        subTitle.text = item.contentSubTitle
        thumbnailImageView.setImage(with: item.contentImageURL)                                            
    }
    
}
