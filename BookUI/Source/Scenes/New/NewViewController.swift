//
//  NewViewController.swift
//  BookUI
//
//  Created by HahnDante on 19/06/2019.
//  Copyright © 2019 한상민. All rights reserved.
//

import UIKit

class NewViewController: UIViewController {
    
    private lazy var dataController = NewDataController(scrollView: self.tableView)
    
    @IBOutlet weak var bottomLine: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.configureNavigationController()
        self.configureDataController()
        self.configureTableView()
        
    }
    
    func configureNavigationController() {
        self.navigationController?.navigationBar.prefersLargeTitles = true
        title = "New"
    }
    
    func configureDataController() {
        dataController.request { [weak self] (isFirstLoading) in
            guard let `self` = self else { return }
            self.tableView.reloadData()
            if self.dataController.results.isEmpty {
                // show emptyview
            }
            if isFirstLoading {
                Animator.defaultListAnimation(targetViews: self.tableView.visibleCells)
            }
        }
    }
    
    func configureTableView() {
        self.tableView.register(UINib(nibName: "NewBooksTableViewCell", bundle: nil), forCellReuseIdentifier: "NewBooksTableViewCell")
        self.tableView.rowHeight  = 200
    }
    
}

extension NewViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataController.results.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let result = dataController.results[indexPath.item]
        let cell = tableView.dequeReusableCell(type: NewBooksTableViewCell.self,
                                               indexPath: indexPath)
        cell.setup(item: result)
        return cell
    }
    
}

extension NewViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let result = dataController.results[indexPath.item]
        let viewController = SceneBuilder.DetailViewController()
        
        if let identifier = result.contentIdentifier {
            viewController.dataController = DetailDataController(identifier: identifier)
            self.navigationController?.pushViewController(viewController, animated: true)
        }
    }
}
