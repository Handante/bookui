//
//  TabBarBuilder.swift
//  BookUI
//
//  Created by HahnDante on 19/06/2019.
//  Copyright © 2019 한상민. All rights reserved.
//

import UIKit

class TabBarBuilder {
    static let mainStoryboardName = "Main"
    
    public enum Menu: String {
        case new = "NewViewController"
        case search = "SearchViewController"
        
        var identifier: String {
            return rawValue
        }
    }
    
    static public var newViewController: NewViewController {
        return UIStoryboard(name: TabBarBuilder.mainStoryboardName,
                                          bundle: nil).instantiateViewController(withIdentifier: Menu.new.identifier) as! NewViewController
    }
    
    static public var searchViewController: SearchViewController {
        return UIStoryboard(name: TabBarBuilder.mainStoryboardName,
                            bundle: nil).instantiateViewController(withIdentifier: Menu.search.identifier) as! SearchViewController
    }
}


