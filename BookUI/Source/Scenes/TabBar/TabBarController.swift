//
//  TabBarController.swift
//  BookUI
//
//  Created by HahnDante on 19/06/2019.
//  Copyright © 2019 한상민. All rights reserved.
//

import UIKit

class TabBarController: UITabBarController {
    
    lazy public var firstViewController: NewViewController = {
        let firstViewController = TabBarBuilder.newViewController
        let title = "New"
        let tabBarItem = UITabBarItem(title: title,
                                      image: nil,
                                      tag: 0)
        firstViewController.tabBarItem = tabBarItem
        return firstViewController
    }()
    
    lazy public var secondViewController: SearchViewController = {
        let secondViewController = TabBarBuilder.searchViewController
        let title = "Search"
        let tabBarItem = UITabBarItem(title: title,
                                      image: nil,
                                      tag: 1)
        secondViewController.tabBarItem = tabBarItem
        return secondViewController
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        let controllers = [firstViewController, secondViewController]
        self.viewControllers = controllers.map({
            UINavigationController(rootViewController: $0)
        })
    }
}

extension TabBarController: UITabBarControllerDelegate {
    func tabBarController(_ tabBarController: UITabBarController, didSelect viewController: UIViewController) {
        print("selected Tabbar \(String(describing: viewController.title))")
    }
}
