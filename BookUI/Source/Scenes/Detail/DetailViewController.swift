//
//  DetailViewController.swift
//  BookUI
//
//  Created by neolyra on 26/06/2019.
//  Copyright © 2019 한상민. All rights reserved.
//

import UIKit
import SafariServices

class DetailViewController: UIViewController {
    
    var dataController: DetailDataController!
    var sectionAndBook = [Section: BookDetail?]()
    var isExapandedTextView = false
    
    enum Section: String {
        case title
        case rating
        case linkButton
        case description
    }
    
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureDetailData()
    }
    
    func configureDetailData() {
        dataController.request { [weak self] (bookDetail) -> (Void) in
            
            if bookDetail.title.count > 0 {
                self?.sectionAndBook[.title] = bookDetail
            }
            
            if bookDetail.rating.count > 0 {
                self?.sectionAndBook[.rating] = bookDetail
            }
            
            if bookDetail.bookURL != nil {
                self?.sectionAndBook[.linkButton] = bookDetail
            }
            
            if bookDetail.desc.count > 0 {
                self?.sectionAndBook[.description] = bookDetail
            }
            self?.tableView.reloadData()
        }
    }
    
}

extension DetailViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    
        if let detailBook = sectionAndBook[.title], indexPath.section == 0 {
            let cell = tableView.dequeReusableCell(type: DetailTitleTableViewCell.self, indexPath: indexPath)
            cell.configureData(item: detailBook!)
            return cell
        }
        
        if let detailBook = sectionAndBook[.rating], indexPath.section == 1 {
            let cell = tableView.dequeReusableCell(type: DetailRatingTableViewCell.self, indexPath: indexPath)
            cell.configureData(item: detailBook!)
            return cell
        }
        
        if let detailBook = sectionAndBook[.linkButton], indexPath.section == 2 {
            let cell = tableView.dequeReusableCell(type: DetailDownloadButtonTableViewCell.self, indexPath: indexPath)
            cell.actionLink = {
                UIApplication.shared.open((detailBook?.bookURL!)!,
                                          options: [:], completionHandler: nil)
            }
            return cell
        }
        
        if let detailBook = sectionAndBook[.description], indexPath.section == 3 {
            let cell = tableView.dequeReusableCell(type: DetailBookDeescriptionTableViewCell.self, indexPath: indexPath)
            cell.configureData(item: detailBook!, isExpanded: isExapandedTextView)
            cell.actionMore = { [weak self] () in
                self?.expandTextView(indexPath: indexPath)
            }
            return cell
        }
        return UITableViewCell()
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.sectionAndBook.keys.count
    }
    
    func expandTextView(indexPath: IndexPath) {
        isExapandedTextView = true
        DispatchQueue.main.async { [weak self] () in
            self?.tableView.beginUpdates()
            self?.tableView.reloadRows(at: [indexPath], with: .fade)
            self?.tableView.endUpdates()
        }
    }
}

extension DetailViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 500
    }
}
