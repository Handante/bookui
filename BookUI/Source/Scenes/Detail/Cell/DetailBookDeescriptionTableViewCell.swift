//
//  DetailBookDeescriptionTableViewCell.swift
//  BookUI
//
//  Created by neolyra on 26/06/2019.
//  Copyright © 2019 한상민. All rights reserved.
//

import UIKit

class DetailBookDeescriptionTableViewCell: UITableViewCell, ReusableView {
    
    static var identifier: String = "DetailBookDeescriptionTableViewCell"
    
    @IBOutlet weak var textView: UITextView!
    @IBOutlet weak var moreButton: UIButton!
    
    var actionMore: (()->())?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.textView.textContainer.maximumNumberOfLines = 4
    }

    func configureData(item: BookDetail, isExpanded: Bool) {
        self.textView.text = item.desc
        
        if isExpanded == true {
            
            self.textView.textContainer.maximumNumberOfLines = 0
            let size = CGSize(width: self.textView.frame.width, height: .infinity)
            let estimatedSize = self.textView.sizeThatFits(size)
            
            self.textView.constraints.forEach { (constraint) in
                if constraint.firstAttribute == .height {
                    constraint.constant = estimatedSize.height
                }
            }
            
            self.moreButton.isHidden = true
        } else {
            self.textView.textContainer.maximumNumberOfLines = 4
            let numberLine = numberOfLines()
            
            if numberLine > 4 {
                self.moreButton.isHidden = false
            } else {
                self.moreButton.isHidden = true
            }
        }
    }
    
    func numberOfLines() -> Int {
        let layoutManager:NSLayoutManager = textView.layoutManager
        let numberOfGlyphs = layoutManager.numberOfGlyphs
        var numberOfLines = 0
        var index = 0
        var lineRange:NSRange = NSRange()
        
        while (index < numberOfGlyphs) {
            layoutManager.lineFragmentRect(forGlyphAt: index, effectiveRange: &lineRange)
            index = NSMaxRange(lineRange);
            numberOfLines = numberOfLines + 1
        }
        return numberOfLines
    }
    
    @IBAction func touchMoreButton(_ sender: Any) {
        actionMore?()
    }
}
