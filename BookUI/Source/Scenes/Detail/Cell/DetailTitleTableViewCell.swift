//
//  DetailTitleTableViewCell.swift
//  BookUI
//
//  Created by HahnDante on 25/06/2019.
//  Copyright © 2019 한상민. All rights reserved.
//

import UIKit

class DetailTitleTableViewCell: UITableViewCell, ReusableView {
    
    static var identifier: String = "DetailTitleTableViewCell"

    @IBOutlet weak var detailImageView: UIImageView!
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var author: UILabel!
    
    func configureData(item: BookDetail) {
        self.detailImageView.setImage(with: item.contentImageURL)
        self.title.text = item.contentTitle
        self.author.text = item.authors
    }

}
