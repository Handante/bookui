//
//  DetailRatingTableViewCell.swift
//  BookUI
//
//  Created by neolyra on 26/06/2019.
//  Copyright © 2019 한상민. All rights reserved.
//

import UIKit

class DetailRatingTableViewCell: UITableViewCell, ReusableView {
    
    static var identifier: String = "DetailRatingTableViewCell"
    
    @IBOutlet weak var publisher: UILabel!
    @IBOutlet weak var pages: UILabel!
    @IBOutlet weak var rating: UILabel!
    @IBOutlet weak var year: UILabel!
    
    func configureData(item: BookDetail) {
        self.publisher.text = "\(item.publisher)"
        self.pages.text = "Pages: \(item.pages)"
        self.rating.text = "Rating: \(item.rating)"
        self.year.text = "Year: \(item.year)"
    }
}
