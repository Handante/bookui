//
//  DetailDownloadButtonTableViewCell.swift
//  BookUI
//
//  Created by neolyra on 26/06/2019.
//  Copyright © 2019 한상민. All rights reserved.
//

import UIKit

class DetailDownloadButtonTableViewCell: UITableViewCell, ReusableView {
    
    static var identifier: String = "DetailDownloadButtonTableViewCell"
    var actionLink: (()->())?
    
    func configureData(item: BookDetail) {
    
    }

    @IBAction func touchStoreLink(_ sender: Any) {
        self.actionLink?()
    }
}
