//
//  DetailDataController.swift
//  BookUI
//
//  Created by neolyra on 26/06/2019.
//  Copyright © 2019 한상민. All rights reserved.
//

import UIKit

class DetailDataController {
    
    private var dataLoader = BookDetailDataLoader<BookDetail>()
    private var identifier: String
    
    init(identifier: String) {
        self.identifier = identifier
    }
    
    func request(completion: @escaping((BookDetail)->(Void))) {
        dataLoader.fetch = { [weak self] (callback) in
            let target = BookDetailTarget.detail(id: self?.identifier ?? "")
            NetworkRequest.request(target: target, completion: callback)
        }
        
        dataLoader.completion = { (book) in
            completion(book)
        }
        
        dataLoader.load()
    }
}


