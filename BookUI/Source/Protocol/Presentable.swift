//
//  Presentable.swift
//  BookUI
//
//  Created by HahnDante on 19/06/2019.
//  Copyright © 2019 한상민. All rights reserved.
//

import Foundation

protocol Presentable {
    
    var contentTitle: String { get }
    
    var contentSubTitle: String { get }
    
    var contentDescription: String { get }
    
    var contentImageURL: URL? { get }
    
    var contentDetailURL: URL? { get }
    
    var contentIdentifier: String? { get }
    
    var cellType: SearchResultCellType { get }
}

extension Presentable {
    
    var contentSubTitle: String {
        return ""
    }
    
    var contentDescription: String {
        return ""
    }
    
    var contentImageURL: URL? {
        return nil
    }
    
    var contentDetailURL: URL? {
        return nil
    }
}

enum SearchResultCellType: String {
    case text
    case smallImage
    case largeImage
    
    var identifier: String {
        return rawValue
    }
}
