//
//  ReusableView.swift
//  BookUI
//
//  Created by HahnDante on 19/06/2019.
//  Copyright © 2019 한상민. All rights reserved.
//

import UIKit

protocol ReusableView {

    static var identifier: String { get }

}

extension UITableView {

    func dequeReusableCell<Cell: UITableViewCell>(type: Cell.Type, indexPath: IndexPath) -> Cell where Cell: ReusableView {
        return dequeueReusableCell(withIdentifier: type.identifier, for: indexPath) as! Cell
    }
}

extension UICollectionView {

    func dequeReusableCell<Cell: UICollectionViewCell>(type: Cell.Type, indexPath: IndexPath) -> Cell where Cell: ReusableView {
        return dequeueReusableCell(withReuseIdentifier: type.identifier, for: indexPath) as! Cell
    }

    func dequeuereusableSupplementaryView<SupplemenTaryView: UICollectionReusableView>(type: SupplemenTaryView.Type, ofKind kind: String, indexPath: IndexPath) -> SupplemenTaryView where SupplemenTaryView: ReusableView {
        return dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: type.identifier, for: indexPath) as! SupplemenTaryView
    }
}
