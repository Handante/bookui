//
//  DetailDataLoader.swift
//  BookUI
//
//  Created by HahnDante on 25/06/2019.
//  Copyright © 2019 한상민. All rights reserved.
//

import Foundation

class BookDetailDataLoader<T: Decodable> {
    
    var fetch: ((@escaping(Result<BookDetail, Error>) -> Void) -> Void)?
    var completion: ((BookDetail) -> Void)?    
    
    func load() {
        self.fetch?() { [weak self] result in
            switch result {
            case .success(let bookDetail):
                self?.completion?(bookDetail)
            case .failure(let error):
                print(error.localizedDescription)
            }

        }
    }
}
