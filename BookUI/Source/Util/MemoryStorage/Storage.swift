//
//  Storage.swift
//  BookUI
//
//  Created by neolyra on 24/06/2019.
//  Copyright © 2019 한상민. All rights reserved.
//

import Foundation

protocol Storage {
    
    associatedtype Value: AnyObject
    
    func isStored(forKey key: String) -> Bool
    
    func value(forKey key: String) -> Value?
    
    func store(value: Value, forKey key: String)
    
    func remove(forKey key: String)
    
    func removeAll()
}
