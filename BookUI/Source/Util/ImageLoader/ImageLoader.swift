//
//  ImageLoader.swift
//  BookUI
//
//  Created by neolyra on 24/06/2019.
//  Copyright © 2019 한상민. All rights reserved.
//

import UIKit

class ImageLoader {
    
    typealias Completion = (Result<UIImage, Error>) -> Void
    
    static let shared = ImageLoader()
    
    private let storage = MemoryStorage<UIImage>()
    private let queue = DispatchQueue(label: "com.imageloader.qeueu", attributes: .concurrent)
    
    func loadImage(with url: URL, completion: @escaping Completion) {
        if let cachedImage = storage.value(forKey: url.absoluteString) {
            completion(.success(cachedImage))
            return
        }
        
        queue.async {
            do {
                let data = try Data(contentsOf: url)
                if let image = UIImage(data: data) {
                    self.storage.store(value: image, forKey: url.absoluteString)
                    completion(.success(image))
                }
                
            } catch {
                completion(.failure(error))
            }
        }
    }
}
