//
//  ListDataLoader.swift
//  BookUI
//
//  Created by HahnDante on 23/06/2019.
//  Copyright © 2019 한상민. All rights reserved.
//

import UIKit

class BookListDataLoader<T: Decodable> {
    
    private weak var scrollView: UIScrollView?
    private var observer: NSKeyValueObservation?
    
    private var startIndex = 1
    private var displayCount = 100
    private var totalCount = 9999
        
    private var isLoading = false
    private var isLoadable: Bool {
        return !isLoading && displayCount < totalCount
    }
    
    var fetch: ((Int, Int, @escaping (Result<List<T>, Error>) -> Void) -> Void)?
    var completion: ((List<T>) -> Void)?
    
    init(scrollView: UIScrollView?, start: Int, display: Int) {
        self.scrollView = scrollView
        startIndex = start
        displayCount = display
        
        observer = scrollView?.observe(\UIScrollView.contentOffset,
                                       options: [.new]) { [weak self] scrollView, change in
                                        guard let `self` = self else { return }
                                        let contentOffset = change.newValue ?? .zero
                                        let islimitLine = scrollView.contentSize.height - contentOffset.y - UIScreen.main.bounds.height < 100
                                        if islimitLine && self.isLoadable && (scrollView.isDragging || scrollView.isDecelerating) {
                                            self.load()
                                        }
            }
        }
    
    deinit {
        observer = nil
    }
    
    func reset() {
        startIndex = 1
        displayCount = 100
        totalCount = 9999
        isLoading = false
        fetch = nil
        completion = nil
    }
    
    func load() {
        guard isLoadable else {
            return
        }
        isLoading = true
        self.fetch?(startIndex, displayCount) { [weak self] result in
            self?.isLoading = false
            switch result {
            case .success(let list):
                self?.startIndex += list.books.count
                self?.totalCount = Int(list.totalCount) ?? 0
                self?.completion?(list)
            case .failure(let error):
                print(error.localizedDescription)
            }
        }
    }

}
