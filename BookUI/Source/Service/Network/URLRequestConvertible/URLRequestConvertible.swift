//
//  URLRequestConvertible.swift
//  BookUI
//
//  Created by HahnDante on 22/06/2019.
//  Copyright © 2019 한상민. All rights reserved.
//

import Foundation

protocol URLRequestComvertible {
    
    func asURLRequest() throws -> URLRequest
}

extension URLRequest: URLRequestComvertible {
    
    func asURLRequest() throws -> URLRequest {
        return self
    }
}
