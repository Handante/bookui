//
//  EndPoint.swift
//  BookUI
//
//  Created by HahnDante on 22/06/2019.
//  Copyright © 2019 한상민. All rights reserved.
//

import Foundation

struct EndPoint: URLRequestComvertible {
    
    let target: TargetType
    
    func asURLRequest() throws -> URLRequest {
        var components = URLComponents(string: target.baseURLString)
        components?.path = target.path
        
        if target.method == .get {
            components?.queryItems = QueryItemConvertor(parameters: target.parameters).convert()
        }
        
        guard let url = components?.url else {
            throw NetworkError.invalidURL
        }
        
        var request = URLRequest(url: url)
        request.httpMethod = target.method.rawValue
        request.allHTTPHeaderFields = target.headers
        if target.method != .get {
            
        }
        
        request.timeoutInterval = 30
        
        return request
    }
}
