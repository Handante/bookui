//
//  NewTarget.swift
//  BookUI
//
//  Created by HahnDante on 23/06/2019.
//  Copyright © 2019 한상민. All rights reserved.
//

import Foundation

enum NewTarget: TargetType {
    
    var method: HTTPMethod {
        return .get
    }
    
    case new(start: Int, dispaly: Int)
    
    var path: String {
        switch self {
        case .new(_, _): return "/1.0/new"
        }
    }
}
