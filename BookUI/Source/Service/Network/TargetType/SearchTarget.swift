//
//  SearchTarget.swift
//  BookUI
//
//  Created by HahnDante on 22/06/2019.
//  Copyright © 2019 한상민. All rights reserved.
//

import Foundation

enum SearchTarget: TargetType {
    
    case search(query: String, page: Int, start: Int, display: Int)
    
    var path: String {
        switch self {
            case .search(let query, let page, _, _): return "/1.0/search/\(query)/\(page)"
        }
    }
    
    var method: HTTPMethod {
        return .get
    }
    
}

