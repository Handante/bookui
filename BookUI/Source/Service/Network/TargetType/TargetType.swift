//
//  TargetType.swift
//  BookUI
//
//  Created by HahnDante on 22/06/2019.
//  Copyright © 2019 한상민. All rights reserved.
//

import Foundation

protocol TargetType {
    
    typealias Parameter = [String: Any]
    
    typealias HeaderField = [String: String]
    
    var baseURLString: String { get }
    
    var path: String { get }
    
    var method: HTTPMethod { get }
    
    var parameters: Parameter? { get }
    
    var headers: HeaderField? { get }
}

extension TargetType {
    
    var baseURLString: String {
        return "https://api.itbook.store"
    }
    
    var path: String {
        return ""
    }
    
    var parameters: Parameter? {
        return nil
    }
    
    var headers: HeaderField? {
        return nil
    }
}
