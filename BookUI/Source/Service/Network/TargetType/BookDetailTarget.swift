//
//  BookDetailTarget.swift
//  BookUI
//
//  Created by neolyra on 26/06/2019.
//  Copyright © 2019 한상민. All rights reserved.
//

import Foundation

enum BookDetailTarget: TargetType {
    
    var method: HTTPMethod {
        return .get
    }
    
    case detail(id: String)
    
    var path: String {
        switch self {
        case .detail(let id): return "/1.0/books/\(id)"
        }
    }
}
