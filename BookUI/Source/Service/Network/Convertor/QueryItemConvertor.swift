//
//  QueryItemConvertor.swift
//  BookUI
//
//  Created by HahnDante on 22/06/2019.
//  Copyright © 2019 한상민. All rights reserved.
//

import Foundation

struct QueryItemConvertor: Convertor {
    
    typealias ConvertedObject = [URLQueryItem]
    
    let parameters: TargetType.Parameter?
    
    func convert() -> [URLQueryItem]? {
        return parameters?.compactMap({ (key, value) in
            URLQueryItem(name: key, value: String(describing: value))
        })
    }
}
