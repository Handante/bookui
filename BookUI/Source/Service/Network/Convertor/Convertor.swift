//
//  Convertor.swift
//  BookUI
//
//  Created by HahnDante on 22/06/2019.
//  Copyright © 2019 한상민. All rights reserved.
//

import Foundation

protocol Convertor {
    
    associatedtype ConvertedObject
    
    func convert() -> ConvertedObject?
}
