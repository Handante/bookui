//
//  HTTPMethod.swift
//  BookUI
//
//  Created by HahnDante on 22/06/2019.
//  Copyright © 2019 한상민. All rights reserved.
//

import Foundation

enum HTTPMethod: String {
    
    case get = "GET"
    
    case post = "POST"

    case put = "PUT"
    
    case delete = "DELETE"
}
