//
//  SessionManager.swift
//  BookUI
//
//  Created by HahnDante on 22/06/2019.
//  Copyright © 2019 한상민. All rights reserved.
//

import Foundation

class SessionManager {
    
    static let `default` = SessionManager(configuration: .default)
    
    private let session: URLSession
    
    private let configuration: URLSessionConfiguration
    
    private var task: URLSessionTask?
    
    init(configuration: URLSessionConfiguration = .default) {
        self.configuration = configuration
        self.session = URLSession(configuration: configuration)
    }
    
    func request(_ request: URLRequestComvertible, completion: @escaping NetworkCompletion) {
        do {
            let urlRequest = try request.asURLRequest()
            task = session.dataTask(with: urlRequest, completionHandler: completion)
            task?.resume()
        } catch {
            completion(nil, nil, error)
        }
    }
    
    func resume() {
        task?.resume()
    }
    
    func suspend() {
        task?.suspend()
    }
    
    func cancle() {
        task?.cancel()
    }
}
