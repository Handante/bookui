//
//  JSONDecoder+Extensions.swift
//  BookUI
//
//  Created by HahnDante on 22/06/2019.
//  Copyright © 2019 한상민. All rights reserved.
//

import Foundation

extension JSONDecoder {
    
    func decode<Model: Decodable>(_ type: Model.Type, from data: Data, keyPath: String) throws -> Model {
        if keyPath.isEmpty {
            return try decode(type, from: data)
        } else {
            do {
                let json = try JSONSerialization.jsonObject(with: data)
                if let nestedJson = (json as AnyObject).value(forKeyPath: keyPath) {
                    let nestedJsonData = try JSONSerialization.data(withJSONObject: nestedJson)
                    return try decode(type, from: nestedJsonData)
                } else {
                    throw NetworkError.keyPathNotFound
                }
            } catch {
                throw error
            }
        }
    }
}
