//
//  BookDetail.swift
//  BookUI
//
//  Created by HahnDante on 25/06/2019.
//  Copyright © 2019 한상민. All rights reserved.
//

import Foundation

struct BookDetail: Decodable {
    
    enum CodingKeys: String, CodingKey {
        case error
        case title
        case subtitle
        case authors
        case publisher
        case majorId = "isbn10"
        case minorId = "isbn13"
        case pages
        case year
        case rating
        case desc
        case price
        case image
        case pdfurl = "url"
    }
    
    let error: String
    let title: String
    let subtitle: String
    let authors: String
    let publisher: String
    let majorId: String
    let minorId: String
    let pages: String
    let year: String
    let rating: String
    let desc: String
    let price: String
    let image: String
    let pdfurl: String
    
    var imageURL: URL? {
        return URL(string: image)
    }
    
    var bookURL: URL? {
        return URL(string: pdfurl)
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        error = try container.decode(String.self, forKey: .error)
        title = try container.decode(String.self, forKey: .title)
        subtitle = try container.decode(String.self, forKey: .subtitle)
        authors = try container.decode(String.self, forKey: .authors)
        publisher = try container.decode(String.self, forKey: .publisher)
        majorId = try container.decode(String.self, forKey: .majorId)
        minorId = try container.decode(String.self, forKey: .minorId)
        pages = try container.decode(String.self, forKey: .pages)
        year = try container.decode(String.self, forKey: .year)
        rating = try container.decode(String.self, forKey: .rating)
        desc = try container.decode(String.self, forKey: .desc)
        price = try container.decode(String.self, forKey: .price)
        image = try container.decode(String.self, forKey: .image)
//        url = try container.decode(String.self, forKey: .url)
        pdfurl = try container.decode(String.self, forKey: .pdfurl)
    }
}

extension BookDetail: Presentable {
    
    var contentIdentifier: String? {
        return minorId
    }
    
    var contentTitle: String {
        return title
    }
    
    var contentSubTitle: String {
        return subtitle
    }
    
    var contentImageURL: URL? {
        return imageURL
    }
    
    var cellType: SearchResultCellType {
        return .largeImage
    }
    
}
