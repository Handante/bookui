//
//  Book.swift
//  BookUI
//
//  Created by HahnDante on 23/06/2019.
//  Copyright © 2019 한상민. All rights reserved.
//

import Foundation

struct Book: Decodable {
    
    enum CodingKeys: String, CodingKey {
        case title
        case subtitle
        case id = "isbn13"
        case price
        case imageURLStr = "image"
        case detailURLStr = "url"
    }
    
    let title: String
    let subTitle: String
    let id: String
    let price: String
    let imageURLStr: String
    let detailURLStr: String
    
    var imageURL: URL? {
        return URL(string: imageURLStr)
    }
    
    var detailURL: URL? {
        return URL(string: detailURLStr)
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        title = try container.decode(String.self, forKey: .title)
        subTitle = try container.decode(String.self, forKey: .subtitle)
        id = try container.decode(String.self, forKey: .id)
        price = try container.decode(String.self, forKey: .price)
        imageURLStr = try container.decode(String.self, forKey: .imageURLStr)
        detailURLStr = try container.decode(String.self, forKey: .detailURLStr)
    }
}

extension Book: Presentable {
    
    var contentIdentifier: String? {
        return id
    }
    
    var contentTitle: String {
        return title
    }
    
    var contentSubTitle: String {
        return subTitle
    }
    
    var contentDetailURL: URL? {
        return detailURL
    }
    
    var contentImageURL: URL? {
        return imageURL
    }
    
    var cellType: SearchResultCellType {
        return .smallImage
    }
    
    
}
