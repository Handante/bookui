//
//  List.swift
//  BookUI
//
//  Created by HahnDante on 23/06/2019.
//  Copyright © 2019 한상민. All rights reserved.
//

import Foundation

struct List<Book: Decodable>: Decodable {
    
    enum CodingKeys: String, CodingKey {
        case books
        case totalCount = "total"
    }
    
    let books: [Book]    
    let totalCount: String
}
