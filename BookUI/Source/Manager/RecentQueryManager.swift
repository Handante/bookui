//
//  RecentQueryManager.swift
//  BookUI
//
//  Created by neolyra on 25/06/2019.
//  Copyright © 2019 한상민. All rights reserved.
//

import Foundation

extension Notification.Name {
    
    static let DidChangeRecentQueries = Notification.Name("DidChangeRecentQueries")
}

struct RecentQueryManager {
    
    private static let maxCount = 10
    private static let recentQueryKey = "recentQueryKey"
    
    static func queries() -> [String] {
        return UserDefaults.standard.value(forKey: recentQueryKey) as? [String] ?? []
    }
    
    static func add(query: String) {
        var queries = self.queries()
        if let index = queries.firstIndex(of: query) {
            queries.remove(at: index)
        } else if queries.count >= maxCount {
            queries.removeFirst()
        }
        
        queries.append(query)
        UserDefaults.standard.set(queries, forKey: recentQueryKey)
        NotificationCenter.default.post(name: .DidChangeRecentQueries, object: nil)
    }
}
